import { inject, bindable } from 'aurelia-framework';
import { EventAggregator } from 'aurelia-event-aggregator';
import { TagService } from '../../tag/services/tag-service';
import constantsMessage from '../../util/constants-message';

@inject(TagService, EventAggregator)
export class TagList {
  @bindable tags;
  @bindable action;

  constructor(tagService, eventAggregator) {
    this.tagService = tagService;
    this.eventAggregator = eventAggregator;
    this.tagService.setViewModel(this);
  }

  bind() {
    this.subscriptionForTagCreated = this.eventAggregator
      .subscribe('tag-to-create', tag => this.createTag(tag));
    this.subscriptionForTagDeleted = this.eventAggregator
      .subscribe('tag-deleted', tag => this.removeTagSelected(tag));
  }

  unbind() {
    this.subscriptionForTagCreated.dispose();
    this.subscriptionForTagDeleted.dispose();
  }

  attached() {
    this.tagService.getTagListByUser();
  }

  /*
  *  Dereference tags after the component is removed from DOM.
  */
  detached() {
    this.tags = [];
  }

  openNewTagModal() {
    if (this.isQuantityOfTagsAllowed()) {
      this.eventAggregator.publish('edit-tag-to-insert', this.tags);
    } else {
      this.tagService.showErrorMessage(constantsMessage.TAG_LIMITED_MESSAGE);
    }
  }

  performAction(tag) {
    this.action({ tag });
  }

  createTag(tag) {
    this.tagService.createTag(tag);
  }

  removeTagSelected(tag) {
    const indexToRemove = this.tags.indexOf(tag);
    this.tags.splice(indexToRemove, 1);
    this.tagSelected = null;
  }

  notifyTagCreated() {
    this.eventAggregator.publish('tag-created');
    this.tagService.getTagListByUser();
  }

  isQuantityOfTagsAllowed() {
    return this.tags.length <= 29;
  }

  setTags(values) {
    this.tags = values;
  }
}
