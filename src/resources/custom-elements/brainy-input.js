import { bindingMode, bindable } from 'aurelia-framework';

export class BrainyInput {
  static id = 0;

  @bindable() mdLabel;
  @bindable() mdBrainyPattern;
  @bindable() mdValidate;
  @bindable() mdValidateError;
  @bindable() mdBrainyMaxLength;
  @bindable() mdBrainyReadOnly = false;
  @bindable() mdRequired = false;
  @bindable() mdType = 'text';

  @bindable({
    defaultBindingMode: bindingMode.twoWay,
  }) mdBrainyValue = '';

  attached() {
    BrainyInput.id += 1;
    const brainyInputId = `brainy-input - ${BrainyInput.id}`;
    this.input.setAttribute('id', brainyInputId);

    if (this.mdValidate === 'true') {
      this.input.classList.add('validate');
    }

    this.label.setAttribute('data-error', this.mdValidateError);
    this.label.setAttribute('for', brainyInputId);
    this.input.setAttribute('pattern', this.mdBrainyPattern);
    this.input.setAttribute('maxlength', this.mdBrainyMaxLength);

    if (this.mdRequired) {
      this.input.setAttribute('required', true);
    }

    this.input.setAttribute('type', this.mdType);
  }

  isTextArea() {
    return this.mdType === 'textarea';
  }
}
