export class NumberFormatValueConverter {
  toView(value) {
    if (isNaN(value)) {
      return 0;
    }

    return parseInt(value, 10);
  }
}
