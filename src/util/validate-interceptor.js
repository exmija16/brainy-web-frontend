import { inject } from 'aurelia-framework';
import { EventAggregator } from 'aurelia-event-aggregator';
import { AuthService } from 'aurelia-auth';
import { Navigation } from './navigation';
import { constants } from './constants';
import localStorageManager from './local-storage-manager';

@inject(Navigation, EventAggregator, AuthService)
export class ValidateInterceptor {

  constructor(navigation, eventAggregator, auth) {
    this.navigation = navigation;
    this.ea = eventAggregator;
    this.auth = auth;
  }

  response(response) {
    this.currentUserAuthenticated();

    let errorMessage;

    if (response.status === constants.SUCCESS_STATUS) {
      return response;
    } else if (response.status === constants.NO_CONTENT) {
      errorMessage = 'No results';
    } else {
      errorMessage = 'An error occurred. Please try again';
    }

    throw errorMessage;
  }

  responseError(response) {
    if (response.status === constants.EXPIRED_TOKEN_STATUS) {
      localStorageManager.removeAuthToken();
      this.navigation.navigateToLogin();
    }

    this.currentUserAuthenticated();
    return response;
  }

  currentUserAuthenticated() {
    if (this.auth.isAuthenticated()) {
      this.ea.publish(constants.UPDATE_NAV_BAR_WHEN_USER_LOGGED);
    }
  }

}
