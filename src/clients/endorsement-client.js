import { inject } from 'aurelia-framework';
import { Client } from './client';

@inject(Client)
export class EndorsementClient {

  constructor(client) {
    this.client = client;
  }

  updateSkillEndorsement(email, skillId, endorsement) {
    const endorsementUrl = `/profileApi/profile/${email}/skill/${skillId}/endorsements`;
    return this.client
        .postTo(endorsementUrl, endorsement)
        .then(response => response.json());
  }

  deleteSkillEndorsement(email, skillId) {
    const endorsementUrl = `/profileApi/profile/${email}/skill/${skillId}/endorsements`;
    return this.client
        .deleteFrom(endorsementUrl)
        .then(response => response.json());
  }
}
