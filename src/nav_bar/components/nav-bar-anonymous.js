import { bindable } from 'aurelia-framework';

export class NavBarAnonymous {
  @bindable changeLanguage;

  change(language) {
    this.changeLanguage(language);
  }
}
