import { bindable, inject } from 'aurelia-framework';
import { UserDetailService } from '../services/user-detail-service';
import { ProfileService } from '../../profile/services/profile-service';
import { BaseComponent } from '../../util/base-component';

@inject(UserDetailService, ProfileService, BaseComponent)
export class UserDetail {

  @bindable() user;
  preloaderAllowed = false;
  progressValue = null;

  constructor(userDetailService, profileService, baseComponent) {
    this.profileService = profileService;
    this.api = userDetailService;
    this.api.setViewModel(this);
    this.baseComponent = baseComponent;
  }

  showSubDetail() {
    if (this.collapsed === undefined) {
      this.preloaderAllowed = true;
      this.collapsed = true;

      this.api
        .getMostEndorsedSkills(this.user.email)
        .then((skills) => {
          this.preloaderAllowed = false;
          this.user.skills = skills;
        });
    } else {
      this.collapsed = !this.collapsed;
    }
  }

  get hasSkills() {
    return this.user.skills && this.user.skills.length > 0;
  }

  goToProfile() {
    this.api.navigateToProfile(this.user);
  }
}
