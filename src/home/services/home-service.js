import { inject, LogManager } from 'aurelia-framework';
import { ProfileClient } from '../../clients/profile-client';
import { BaseComponent } from '../../util/base-component';
import localStorageManager from '../../util/local-storage-manager';
import { constants } from '../../util/constants';

@inject(BaseComponent, ProfileClient)
export class HomeService {

  constructor(baseComponent, profileClient) {
    this.baseComponent = baseComponent;
    this.profileClient = profileClient;
    this.logger = LogManager.getLogger(HomeService.name);
  }

  setViewModel(value) {
    this.viewModel = value;
  }

  searchFromAPI(query, last, items) {
    if (last === constants.DEFAULT_LAST_INDEX) {
      this.baseComponent.showProgressHub();
    }
    return this.profileClient.retrieveListOfProfiles(query, last, items)
      .then(users => this.processSearchResult(users, last))
      .catch((error) => {
        this.logger.error('Details of the error', error);
        this.baseComponent.showMessageError();
      })
      .then(() => {
        if (last === constants.DEFAULT_LAST_INDEX) {
          this.baseComponent.dismissProgressHub();
        }
      });
  }

  advancedSearchFromAPI(name, skills, headquarter, locations, language, position, last, items) {
    this.baseComponent.showProgressHub();

    return this.profileClient.retrieveListOfProfilesAdvanced(name, skills,
       headquarter, locations, language, position, last, items)
      .then(users => this.processSearchResult(users, last))
      .catch((error) => {
        this.logger.error('Details of the error', error);
        this.baseComponent.showMessageError();
      })
      .then(() => {
        this.baseComponent.dismissProgressHub();
      });
  }

  markCurrentUser(users) {
    const currentUser = users.find(user => (
      localStorageManager.isCurrentUser(user.email)
    ));

    if (currentUser) {
      currentUser.setCurrentUser(true);
    }
  }

  getPositions() {
    return this.profileClient.retrieveListOfPositions()
    .then(positions => positions);
  }

  getEnglishLevels() {
    return this.profileClient.retrieveListOfEnglishLevels()
    .then(englishLevels => englishLevels);
  }

  processSearchResult(users, last) {
    let total = users.length;
    if (total > 0) {
      if (last === constants.DEFAULT_LAST_INDEX) {
        total = users[0].id;
        users.shift();
        const message = `There were ${total} people that match your search`;
        this.baseComponent.showMessageSuccess(message);
      }
      this.markCurrentUser(users);
    }
    if (last === 0) {
      this.viewModel.setResults(users, total);
    } else {
      this.viewModel.addResults(users);
    }
  }
}
