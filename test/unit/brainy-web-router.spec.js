import { BrainyWebRouter } from '../../src/routing/brainy-web-router';
import TestingPromiseHelper from './helpers/testing-promise-helper';

describe('WebRouter', () => {
  let brainyWebRouter;
  let router;
  let config;
  let promiseHelper;
  let configureCallback;

  beforeEach(() => {
    config = { addAuthorizeStep: () => {}, map: () => {} };
    promiseHelper = new TestingPromiseHelper();

    router = {
      configure: (callback) => {
        configureCallback = callback;
        return promiseHelper.promise;
      },
    };

    brainyWebRouter = new BrainyWebRouter(router);
  });

  it('should configure the router', (done) => {
    brainyWebRouter.configure().then(() => {
      configureCallback(config);
      expect(config.title).toBe('Brainy');
      done();
    });

    promiseHelper.decider.resolve();
  });
});
